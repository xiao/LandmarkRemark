#import "LRMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <FirebaseAuth/FirebaseAuth.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import "LMNote.h"
#import "LMUser.h"
#import "LMMapCalloutDetailView.h"
#import "LMAnnotation.h"
#import "LMAllNotesViewController.h"
#import "LMSingleSiteNotesViewController.h"

@interface LRMapViewController ()<MKMapViewDelegate, LMMapCalloutDetailViewDelegate>
@property (nonatomic, strong) LMUser *lmuser;
@property (nonatomic, strong) FIRDatabaseReference *ref;
@property (nonatomic, strong) MKUserLocation *currentLocation;
@property (strong, nonatomic) NSDictionary *items;
@property (nonatomic, strong) NSMutableArray<MKPointAnnotation *> *annotations;
@property (nonatomic, strong) LMAnnotation *selectedAnnotation;
@property (weak, nonatomic) IBOutlet UIButton *locationMeBtn;

@end

@implementation LRMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Map";
    // init mapview
    self.locationMap.delegate = self;
    self.locationMap.showsCompass = YES;
    self.locationMap.showsUserLocation = YES;
    
    // init annotaion array
    _annotations = [NSMutableArray array];
    
    // setup navation bar
    [self addRightNavItem];
    [self addLeftNavItem];
    
    // init firbase database
    self.ref = [[FIRDatabase database] referenceWithPath:@"noteList"];
    
    // fetch current user
    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        if (user) {
            self.lmuser = [[LMUser alloc]initWithFIRUser:user];
        }
    }];
    
    // fetch data from server
    [[self.ref queryOrderedByKey] observeEventType:FIRDataEventTypeValue  withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isEqual:[NSNull null]]) {
            self.items = snapshot.value;
            [self addAnnotationOnMap];
        }
    }];
    
    //set up find my current location
    [self setLocationButton];
}

#pragma mark -- UI Setup
- (void)addRightNavItem {
    UIBarButtonItem *editBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNote)];
    [editBarBtn setTintColor: [UIColor  whiteColor]];
    self.navigationItem.rightBarButtonItem = editBarBtn;
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]}];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)addLeftNavItem {
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleDone target:self action:@selector(logout)];
    [logoutButton setTintColor: [UIColor  whiteColor]];
    self.navigationItem.leftBarButtonItem = logoutButton;
}

- (void)setLocationButton {
    UIImage *locationImage = [[UIImage imageNamed:@"Tag"]  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_locationMeBtn setImage:locationImage forState:UIControlStateNormal];
    [_locationMeBtn setTintColor:[UIColor blackColor]];
}

#pragma mark -- UI Actions
- (void)logout {
    [self dismissViewControllerAnimated:YES completion:^{
        NSError *error;
        [[FIRAuth auth] signOut:&error];
    }];
}

- (IBAction)toMyLocation:(id)sender {
    [self showUserLocation];
}

- (void)addNote {
    // must enable location service to save note
    if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || _currentLocation == nil) {
        [self showLocationServiceAlert];
        return;
    }
    
    // To user current location on the map
    [self showUserLocation];
   
    
    UIAlertController *alerController = [UIAlertController alertControllerWithTitle:@"Add note" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alerController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Note";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alerController addAction:cancelAction];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *note = alerController.textFields[0].text;
        if (note) {
            [self saveNote:note];
        }
        NSLog(@"%@",note);
    }];
    [alerController addAction:okAction];
    
    [self presentViewController:alerController animated:YES completion:nil];
}

- (IBAction)listAllNote:(id)sender {
    if (self.items) {
        LMAllNotesViewController *noteViewController = [[UIStoryboard  storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LMAllNotesViewController"];
        noteViewController.allNotes = self.items;
        [self.navigationController pushViewController:noteViewController animated:YES];
    } else {
        
        
    }
}


- (void)saveNote:(NSString *)note {
    __block NSString *streeAddress;
    // cover current location to address
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:_currentLocation.location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        CLPlacemark *mark = [placemarks objectAtIndex:0];
        NSString *postcode = mark.postalCode;
        NSString *country = mark.country;
        NSString *state = mark.administrativeArea;
        NSString *street = mark.thoroughfare;
        NSString *suburb = mark.locality;
        if (street.length) {
            streeAddress = [NSString stringWithFormat:@"%@, %@ %@ %@ %@", street, suburb, state, postcode, country];
        }else {
            streeAddress = [NSString stringWithFormat:@"%@ %@ %@ %@", suburb, state, postcode, country];
        }
        // cover current stree address to Geocode, save notes based on stree location
        [geocoder geocodeAddressString:streeAddress completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            CLPlacemark *mark = [placemarks objectAtIndex:0];
            CLLocation *location = mark.location;
            LMNote *lmnote = [[LMNote alloc]init];
            lmnote.user = _lmuser;
            lmnote.note = note;
            lmnote.latitude =  location.coordinate.latitude;
            lmnote.longitude =  location.coordinate.longitude;
            lmnote.address = streeAddress;
            __block NSArray *siteNotes = [self.items objectForKey:streeAddress];
            if (siteNotes.count) {
                NSMutableArray *notes = [NSMutableArray arrayWithArray:siteNotes];
                [notes addObject:lmnote.toJsonObject];
                siteNotes = [notes copy];
            }else {
                NSMutableArray *notes = [NSMutableArray new];
                [notes addObject:lmnote.toJsonObject];
                siteNotes = [notes copy];
            }
            FIRDatabaseReference *childref = [self.ref child:streeAddress];
            [childref setValue:siteNotes withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [self displayNotes:siteNotes];
                }
            }];
       }];
    }];
}

// Display all the note of the street where you are
- (void)displayNotes:(NSArray *)notes{
    LMSingleSiteNotesViewController *singleVC = [[LMSingleSiteNotesViewController alloc] initWithNibName:nil bundle:nil];
    singleVC.notes = notes;
    [self.navigationController pushViewController:singleVC animated:YES];
}

- (void)addAnnotationOnMap {
    for (NSString *key in self.items.keyEnumerator ) {
        NSArray *siteNotes = [self.items objectForKey:key];
        if (siteNotes.count) {
            LMAnnotation *annotation = [[LMAnnotation alloc] initWithNote:siteNotes];
            [_annotations addObject:annotation];
        }
    }
    
    [self.locationMap addAnnotations:_annotations];
}

- (void)showUserLocation {
    if (_currentLocation) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(_currentLocation.coordinate, 1000, 1000);
        MKCoordinateRegion regionDelMapa = [self.locationMap regionThatFits:region];
        [self.locationMap setRegion:regionDelMapa animated:YES];
    }
}


#pragma mark -- MapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (userLocation)
    {
        _currentLocation = userLocation;
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    LMAnnotation *lmannotaion = annotation;
    NSString *title = lmannotaion.address;
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:title];
    
    if (view == nil) {
        view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:title];
        
    } else {
        view.annotation = lmannotaion;
    }
    
    LMMapCalloutDetailView *calloutView =[[LMMapCalloutDetailView alloc]initWithFrame:CGRectZero];
    [calloutView addConstraint:[NSLayoutConstraint constraintWithItem:calloutView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:300]];
    [calloutView addConstraint:[NSLayoutConstraint constraintWithItem:calloutView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100]];
    
    calloutView.addressLabel.text = lmannotaion.address;
    calloutView.notesLabel.text = lmannotaion.noteCount;
    calloutView.notes = lmannotaion.notes;
    calloutView.delegate = self;
    view.detailCalloutAccessoryView = calloutView;
    view.canShowCallout = YES;
    
    UIImage *iconImage = [UIImage imageNamed:@"flag"];
    view.image = iconImage;
    return view;
}


#pragma mark -- LMMapCalloutDetailViewDelegate
- (void)detailsRequtestForSite:(NSArray *)notes {
    [self displayNotes:notes];
}

#pragma mark -- Location Service enable alert
- (void)showLocationServiceAlert {
    NSString *title;
    if ([CLLocationManager locationServicesEnabled]) {
        title = @"Turn on location services in Setting > Privacy > Location Service > LandmarkRemark > While Using the App.";
    }else {
        title = @"Turn on location services in Setting > Privacy to allow Maps to determine your current location";
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Open Location Service" message: title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:[NSDictionary new] completionHandler:nil];
    }];
    
    [alertController addAction:settingAction];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
