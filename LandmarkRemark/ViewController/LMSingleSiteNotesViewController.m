#import "LMSingleSiteNotesViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>
#import "LMUser.h"
#import "LMNote.h"
#import "LMSingleSiteHeaderView.h"

@interface LMSingleSiteNotesViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) LMUser *user;

@end

@implementation LMSingleSiteNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Site Notes";
    NSArray* reversedArray = [[self.notes reverseObjectEnumerator] allObjects];
    self.notes = reversedArray;
    [self setuptableView];
    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        if (user) {
            self.user = [[LMUser alloc]initWithFIRUser:user];
            [self.tableView reloadData];
        }
    }];
}

- (void)setuptableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_tableView);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:nil views:views]];
    UIView * footer = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footer;
    [self.tableView registerClass:[LMSingleSiteHeaderView class] forHeaderFooterViewReuseIdentifier:@"LMSingleSiteHeaderView"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma  mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableviewcell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"tableviewcell"];
    }
    
    LMNote *noteItem = [[LMNote alloc] initWithJSON:[self.notes objectAtIndex:indexPath.row]];
    NSString *emailString = noteItem.user.email;
    if ([self.user.uuid isEqualToString:noteItem.user.uuid]) {
        emailString = @"Me";
    }
    cell.textLabel.text = noteItem.note;
    cell.detailTextLabel.text = emailString;
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LMSingleSiteHeaderView *headerView = nil;
    headerView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"LMSingleSiteHeaderView"];
    if (self.notes.count) {
        LMNote *noteItem = [[LMNote alloc] initWithJSON:[self.notes objectAtIndex:0]];
        headerView.title = noteItem.address;
    }
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 70;
}

@end
