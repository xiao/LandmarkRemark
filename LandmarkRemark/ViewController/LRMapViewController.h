#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LRMapViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *locationMap;
@end

