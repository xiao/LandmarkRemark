#import "LMLoginViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>

@interface LMLoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldLoginEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLoginPassword;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@end

@implementation LMLoginViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _textFieldLoginEmail.delegate = self;
    _textFieldLoginPassword.delegate = self;
    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        if (user) {
            [self performSegueWithIdentifier:@"LMLogin" sender:nil];
        }
    }];
}

- (IBAction)loginDidTouch:(id)sender {
    NSString *username = self.textFieldLoginEmail.text;
    NSString *password = self.textFieldLoginPassword.text;
    
    if (!username.length) {
        [self popupMessageWithTitle:@"Empty username" message:@"Input the username"];
        return;
    }
    
    if (!password.length) {
        [self popupMessageWithTitle:@"Invalid password" message:@"Input correct passwor"];
        return;
    }
    
    
    [[FIRAuth auth] signInWithEmail:username password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        if (user) {
            [self performSegueWithIdentifier:@"LMLogin" sender:nil];
        } else {
            [self popupMessageWithTitle:@"Invalid user" message:@"Try again"];
        }
    }];
}


- (IBAction)signUpDidTouch:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Register"
                                                                             message:@"Register"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *mailAddress = [self removeSpaceAndLowercase:alertController.textFields[0].text];
        NSString *password = [alertController.textFields[1].text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (!mailAddress.length || !password.length) {
            [self showAlertLabel:@"Register with both password and email"];
            return;
        }
        
        [[FIRAuth auth] createUserWithEmail:mailAddress password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
            if (user) {
                [[FIRAuth auth] signInWithEmail:mailAddress password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                    if (user) {
                        [self performSegueWithIdentifier:@"LMLogin" sender:nil];
                    } else {
                        [self popupMessageWithTitle:@"Invalid user" message:@"Try again"];
                    }
                }];
                return;
            }
            [self showAlertLabel:error.localizedDescription];
        }];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder  = @"Enter your email";
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter your password";
        textField.secureTextEntry = YES;
    }];
    
    [alertController addAction:saveAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//display alert label when failed to create user, and will be hidden antomatically in 16 sec
- (void)showAlertLabel:(NSString *)errorMessage {
    self.alertLabel.hidden = NO;
    self.alertLabel.text = errorMessage;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.alertLabel.hidden = YES;
        });
    });
    
}



#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.textFieldLoginEmail) {
        [self.textFieldLoginPassword becomeFirstResponder];
    }
    
    if (textField == self.textFieldLoginPassword) {
        [self.textFieldLoginPassword resignFirstResponder];
    }
    return true;
}

#pragma mark -- General
- (void)popupMessageWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *OKButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:OKButton];
    [self presentViewController:alertController animated:NO completion:nil];
}

- (NSString *)removeSpaceAndLowercase:(NSString*)characters {
    if (characters) {
        NSString *value = [characters stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (value.length) {
            return [value lowercaseString];
        }
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
