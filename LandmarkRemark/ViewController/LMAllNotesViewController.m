#import "LMAllNotesViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>
#import "LMNote.h"
#import "LMUser.h"
#import "LMSingleSiteHeaderView.h"

@interface LMAllNotesViewController ()<UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (strong, nonatomic) LMUser *user;
@property (strong, nonatomic) NSDictionary *searchItems;
@property (strong, nonatomic) NSDictionary *myNotes;

@end

@implementation LMAllNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setuptableview];
    
    [[FIRAuth auth] addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        if (user) {
            self.user = [[LMUser alloc]initWithFIRUser:user];

            
            //Create all notes array, my notes arry
            NSMutableDictionary *filterDict = [NSMutableDictionary dictionary];
            for (NSString *key in self.allNotes.allKeys) {
                NSArray *thisNotes = [self.allNotes objectForKey:key];
                NSMutableArray *filterArray = [NSMutableArray array];
                for (NSDictionary *dict in thisNotes) {
                    LMNote *noteItem = [[LMNote alloc] initWithJSON:dict];
                    if ([noteItem.user.uuid isEqualToString:self.user.uuid]) {
                        [filterArray addObject:dict];
                    }
                }
                if (filterArray.count) {
                    [filterDict setObject:filterArray forKey:key];
                }
            }
            self.myNotes = [filterDict copy];
            [self.tableview reloadData];
        }
    }];
    
    self.searchBar.delegate = self;
    self.searchItems = [self getCurrentData];
    [self.segment addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
}

- (void)setuptableview {
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    self.edgesForExtendedLayout = UIRectEdgeNone; //hide header gap
    [self.tableview registerClass:[LMSingleSiteHeaderView class] forHeaderFooterViewReuseIdentifier:@"LMSingleSiteHeaderView"];
    
}

- (void)segmentSelected {
    self.searchBar.text=@"";
    [self.searchBar endEditing:YES];
    self.searchItems = [self getCurrentData];
    [self.tableview reloadData];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchTitle = [[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    NSDictionary *sectionRecords = [self getCurrentData];
    NSMutableDictionary *filterDict = [NSMutableDictionary dictionary];
    for (NSString *key in sectionRecords.allKeys) {
        NSArray *thisNotes = [sectionRecords objectForKey:key];
        NSMutableArray *filterArray = [NSMutableArray array];
        if ([key.lowercaseString containsString:searchTitle]) {
            //if address contains search characters, add all notes under the site to search result
            [filterDict setObject:thisNotes forKey:key];
        } else {
            //other wise add these record based on container text or user-email
            for (NSDictionary *dict in thisNotes) {
                LMNote *noteItem = [[LMNote alloc] initWithJSON:dict];
                LMUser *user = noteItem.user;
                if (noteItem.note.length && user.email.length) {
                    if ([noteItem.note.lowercaseString containsString:searchTitle] || [user.email.lowercaseString containsString:searchTitle]) {
                        [filterArray addObject:dict];
                    }
                }
                
            }
            if (filterArray.count) {
                [filterDict setObject:filterArray forKey:key];
            }
        }
        
    }
    self.searchItems = [filterDict copy];
    [self.tableview reloadData];
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    NSString *searchTitle = [[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    if (!searchTitle.length) {
        self.searchItems = [self getCurrentData];
        [self.tableview reloadData];
    }
}

#pragma mark - UITableviewdatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSDictionary *showUpNotes = self.searchItems;
    return showUpNotes.allKeys.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *showUpNotes = self.searchItems;
    NSArray *keys = [showUpNotes allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSString *key = [sortedArray objectAtIndex:section];
    NSArray *thisNotes = [showUpNotes objectForKey:key];
    return thisNotes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableviewcell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"tableviewcell"];
    }
    NSDictionary *showUpNotes = self.searchItems;
    NSArray *keys = [showUpNotes allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSString *key = [sortedArray objectAtIndex:indexPath.section];
    NSArray *thisNotes = [showUpNotes objectForKey:key];
    NSArray* reversedArray = [[thisNotes reverseObjectEnumerator] allObjects];
    thisNotes = reversedArray;
    
    LMNote *noteItem = [[LMNote alloc] initWithJSON:[thisNotes objectAtIndex:indexPath.row]];
    NSString *emailString = noteItem.user.email;
    if ([self.user.uuid isEqualToString:noteItem.user.uuid]) {
        emailString = @"Me";
    }
    
    cell.textLabel.text = noteItem.note;
    cell.detailTextLabel.text = emailString;
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LMSingleSiteHeaderView *headerView = nil;
    headerView = [self.tableview dequeueReusableHeaderFooterViewWithIdentifier:@"LMSingleSiteHeaderView"];
    
    NSDictionary *showUpNotes = self.searchItems;
    NSArray *keys = [showUpNotes allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSString *key = [sortedArray objectAtIndex:section];
    NSArray *thisNotes = [showUpNotes objectForKey:key];
    if (thisNotes.count) {
        LMNote *noteItem = [[LMNote alloc] initWithJSON:[thisNotes objectAtIndex:0]];
        headerView.title = noteItem.address;
    }
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 70;
}

#pragma mark - All Notes/ My Notes

- (NSDictionary*)getCurrentData {
    NSDictionary *showUpNotes ;
    if (self.segment.selectedSegmentIndex == 0) {
        showUpNotes = self.allNotes;
    }else {
        showUpNotes = self.myNotes;
    }
    return showUpNotes;
}
@end
