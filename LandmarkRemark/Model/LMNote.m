#import "LMNote.h"

@implementation LMNote


- (instancetype)initWithDatasnap:(FIRDataSnapshot *)datasnapshot {
    self = [super init];
    if (self) {
        [self parseDatasnapshot:datasnapshot];
    }
    return self;
}

- (instancetype)initWithJSON:(NSDictionary *)obj {
    self =[super init];
    if (self) {
        [self jsonToObj:obj];
    }
    return self;
}

- (void)parseDatasnapshot:(FIRDataSnapshot *)snapshot {
    NSDictionary *dict = snapshot.value;
    if (dict) {
        [self jsonToObj:dict];
        self.ref = snapshot.ref;
    }
}

- (void)jsonToObj:(NSDictionary *)dict {
    self.latitude = [[dict objectForKey:@"latitude"] doubleValue];
    self.longitude = [[dict objectForKey:@"longitude"] doubleValue];
    self.note =  [dict objectForKey:@"message"];
    self.address =  [dict objectForKey:@"address"];
    
    NSDictionary *user = [dict objectForKey:@"user"];
    if (user) {
        self.user = [[LMUser alloc] initWithDict:user];
    }
}

- (NSDictionary *)toJsonObject {
    NSMutableDictionary *note = [NSMutableDictionary new];
    [note setObject:@(self.longitude) forKey:@"longitude"];
    [note setObject:@(self.latitude) forKey:@"latitude"];
    [note setObject:self.user.toJson forKey:@"user"];
    [note setObject:self.address forKey:@"address"];
    [note setObject:self.note forKey:@"message"];
    return note;
}
@end
