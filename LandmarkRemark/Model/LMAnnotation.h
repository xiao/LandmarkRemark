#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LMAnnotation : MKPointAnnotation
@property (strong, nonatomic) NSArray *notes;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *noteCount;

- (instancetype)initWithNote:(NSArray*)noteArray;
@end
