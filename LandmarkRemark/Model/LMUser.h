#import <Foundation/Foundation.h>
@import FirebaseAuth;
@interface LMUser : NSObject
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *email;

- (instancetype)initWithFIRUser:(FIRUser *)user;
- (instancetype)initWithEmail:(NSString *)email  withUID:(NSString *)uuid;
- (instancetype)initWithDict:(NSDictionary *)dict;
- (NSDictionary *)toJson;
@end
