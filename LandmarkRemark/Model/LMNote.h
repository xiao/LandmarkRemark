#import <Foundation/Foundation.h>
#import "LMUser.h"
@import FirebaseDatabase;
@interface LMNote : NSObject
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) LMUser *user;
@property (nonatomic, strong) FIRDatabaseReference *ref;
@property (nonatomic, strong) NSString *address;

- (NSDictionary *)toJsonObject;
- (instancetype)initWithDatasnap:(FIRDataSnapshot *)datasnapshot;
- (instancetype)initWithJSON:(NSDictionary *)obj;

@end
