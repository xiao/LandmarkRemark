#import "LMUser.h"
@implementation LMUser

- (instancetype)initWithFIRUser:(FIRUser *)user {
    self = [super init];
    if (self) {
        self.email = user.email;
        self.uuid = user.uid;
    }
    return self;
}

- (instancetype)initWithEmail:(NSString *)email  withUID:(NSString *)uuid {
    self = [super init];
    if (self) {
        self.email = email;
        self.uuid = uuid;
    }
    return self;
}

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        [self parseDict:dict];
    }
    return self;
}

- (void)parseDict:(NSDictionary *)dict {
    if (dict) {
        self.email = [dict objectForKey:@"email"];
        self.uuid = [dict objectForKey:@"uid"];
    }
}

- (NSDictionary *)toJson {
    NSMutableDictionary *user = [NSMutableDictionary new];
    [user setObject:self.email forKey:@"email"];
    [user setObject:self.uuid forKey:@"uid"];
    return user;
}

@end
