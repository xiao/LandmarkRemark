#import "LMAnnotation.h"
#import <MapKit/MapKit.h>
#import "LMNote.h"

@implementation LMAnnotation
- (instancetype)initWithNote:(NSArray*)noteArray {
    if (self = [self init]) {
        _notes = noteArray;
        LMNote *note = [[LMNote alloc] initWithJSON:[_notes objectAtIndex:0]];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(note.latitude, note.longitude);
        self.coordinate = coordinate;
        self.title = @"Site note";
        self.noteCount = [NSString stringWithFormat:@"There are %lu notes",(unsigned long)noteArray.count];
        self.address = [NSString stringWithFormat:@"%@", note.address];
    }
    
    return self;
}

@end
