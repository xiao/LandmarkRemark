#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ActiveLocationMonitor : NSObject <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readonly) BOOL isMonitoring;

+(ActiveLocationMonitor *)sharedInstance;
- (void)startMonitoring;
- (void)stopMonitoring;

@end
