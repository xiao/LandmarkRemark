#import "ActiveLocationMonitor.h"
@interface ActiveLocationMonitor()
@property (nonatomic, strong) NSDate *lastTimeStamp;
@end
@implementation ActiveLocationMonitor


+ (ActiveLocationMonitor *)sharedInstance
{
    static ActiveLocationMonitor *sharedActiveLocationMonitor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedActiveLocationMonitor = [[self alloc] init];
    });
    return sharedActiveLocationMonitor;
}


-(ActiveLocationMonitor *)init{
    
    self = [super init];
    if(self) {
        self.locationManager = nil;
    }
    
    return self;
}

#pragma mark - Setter/Getter
- (BOOL)isMonitoring
{
    return (_locationManager != nil);
}

- (void)startMonitoring
{
    if(self.isMonitoring == NO) {
        NSLog(@"Active Location Monitor: Start monitoring");
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = 100.0f;
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:100.0f - 1.0f timeout:10.0f];      // every 10 secs;
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
    
}


- (void)stopMonitoring
{
    NSLog(@"Active Monitor: Stop monitoring");
    if(_locationManager) {
        [self.locationManager stopUpdatingLocation];
    }
    self.locationManager = nil;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *lastLocation = [locations lastObject];
    NSDate *timeStamp = lastLocation.timestamp;
    if(self.lastTimeStamp != nil) {
        if(timeStamp.timeIntervalSince1970 - self.lastTimeStamp.timeIntervalSince1970 >= (12.0f-1.0f)) {
            NSLog(@"Last Active Location: %@", lastLocation);
            self.lastTimeStamp = timeStamp;
        }
    }
    else {
        NSLog(@"Last Active Location: %@", lastLocation);
        self.lastTimeStamp = timeStamp;
    }
}
@end
