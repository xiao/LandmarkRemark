#import "AppDelegate.h"
#import "ActiveLocationMonitor.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseCore/FIRApp.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    [[FIRDatabase database] setPersistenceEnabled:YES];
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    //disable location service
    [[ActiveLocationMonitor sharedInstance] stopMonitoring];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    //enable location service
    [self startLocationMointors];
}

- (void)startLocationMointors {
    if (![ActiveLocationMonitor sharedInstance].isMonitoring) {
        [[ActiveLocationMonitor sharedInstance] startMonitoring];
    }
}
@end
