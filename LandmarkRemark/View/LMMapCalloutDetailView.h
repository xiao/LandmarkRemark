#import <UIKit/UIKit.h>
@protocol LMMapCalloutDetailViewDelegate <NSObject>

-(void)detailsRequtestForSite:(NSArray *)notes;

@end

@interface LMMapCalloutDetailView : UIView
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *notesLabel;
@property (nonatomic, strong) UIButton *detailButton;
@property (nonatomic, strong) NSArray *notes;
@property (nonatomic, weak) id<LMMapCalloutDetailViewDelegate> delegate;
@end
