#import "LMSingleSiteHeaderView.h"

@interface LMSingleSiteHeaderView()
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation LMSingleSiteHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [self addSubview:_titleLabel];
        self.contentView.backgroundColor = [UIColor lightGrayColor];
        
    }
    return self;

}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel);
    NSMutableArray *constraints = [NSMutableArray array];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_titleLabel]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(==10)-[_titleLabel]-(==10)-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:constraints];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    _titleLabel.text = _title;
}
@end
