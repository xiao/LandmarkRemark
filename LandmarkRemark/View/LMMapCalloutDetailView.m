#import "LMMapCalloutDetailView.h"

@implementation LMMapCalloutDetailView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _addressLabel.numberOfLines = 0;
        [self addSubview: _addressLabel];
        _notesLabel = [[UILabel alloc] init];
        _notesLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview: _notesLabel];
        
        _detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _detailButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_detailButton setTitle:@"Details" forState:UIControlStateNormal];
        [_detailButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [_detailButton addTarget:self action:@selector(detailSelected) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_detailButton];
    
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSMutableArray *constrains = [NSMutableArray array];
    NSDictionary *views = NSDictionaryOfVariableBindings(_addressLabel, _notesLabel, _detailButton);
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_addressLabel]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_notesLabel]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_detailButton]|" options:0 metrics:nil views:views]];
    [constrains addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_addressLabel(44)][_notesLabel][_detailButton(30)]|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:constrains];
}

- (void)detailSelected {
    if ([_delegate respondsToSelector:@selector(detailsRequtestForSite:)]) {
        [_delegate detailsRequtestForSite:self.notes];
    }
}



@end
