# Landmark-Remakr Project

The explicit requirements are:
 - As a user(of the application) I can see my current location on a map [1 man-hours]
 - As a user I can save a short note at my current location,   [4 man-hours]
 - As a user I can see notes that I have saved at the location they were saved on the map [2 man-hours]
 - As a user I can see the location, text, and user-name of notes other users have saved. [1 man-hours]
 - As a user I have the ability to search for a note based on contained text or user-name [2 man-housr]

# Approach to the solution
 - User can sign in, sign up 
 - User can see current location on the map
 - Need to turn on location service, otherwise alert user, they need to open the location serveice
 - User can save the note at current street, can save multiple notes at the same street. Click annotation on map, can see all the notes which are created by users. 
 - If User move to another street, and there is no note created at that street. Adding note will create a new annotation on the map.
 - User can see all notes, or my notes. user can search notes in my notes list. user can search notes in all notes list.
 - All data will persisted on firebase
 - Data structure : [LandmarkRemark/LandmarkRemark/dataExample.json](https://gitlab.com/xiao/LandmarkRemark/blob/master/LandmarkRemark/Asserts/dataExample.json)

# Architecutre
The app is designed using MVM . - Grouping of notes at the same street. Able to see all the notes at same street in the tableview.


# Test account
These are the existing test account, you can create you own account by clicking sign up button in the login page.

 - | 1@happy.com | test123 |
 - | 2@happy.com | test1234 |
 - | 3@happy.com | test123 |

# Backend support
Using Firebase(https://console.firebase.google.com/) database as backend Support.  

# Simulator
If running in simulator, click simulator -> Debug -> Location -> Custom Location
example:
-33.886823, 151.211012
-33.886730, 151.210249
-33.813691, 150.989387

# Fake address for testing
 - Parramatta street in parramatta
 - Adelaide street in centre
 - Alexandria Suburb in Redfern
 - Cooper street in centre
 - Devonshire street in centre
 - Waterlloo street in centre
